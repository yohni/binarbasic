#!/usr/bin/ruby -w

$input1 = 0
$input2 = 0
$operator = "x"
class Operation
  def initialize(in1, in2, op)
    $input1 = in1
    $input2 = in2
    $operator = op
  end
  def exec()
    case $operator
    when "+"
      puts $input1 + $input2
    when "-"
      puts $input1 - $input2
    when "*"
      puts $input1 * $input2
    when "/"
      puts $input1 / $input2
    else
      puts "just operator: +,-,*,/"
    end
  end
end

kalkulator = Operation. new(3,4,"/")
kalkulator.exec